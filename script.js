"use strict" ;

// 1. Як можна сторити функцію та як ми можемо її викликати?

// Function declaration - визначає ім'я функції за допомогою ключового слова `function`. 
// Зазвичай вони піднімаються це означає, що вони доступні в усій області, в якій вони визначені, навіть якщо вони оголошені після їх використання.

// Function Expression - передбачає визначення функції та присвоювання її змінній. Ці функції можуть бути анонімними або мати ім'я. 
// Вони не піднімаються (не доступні до виклику) і можуть бути використані лише після того, як їх призначено.

// а) анонімні функції

// const addition = function(a, b) {
//     let result = a + b;
//     return result;
//   };
  
//    let result = addition(5, 5);
//     console.log(result); 
  
// b) іменована функція 

// const multiple = function multiple(a, b) {
//     let result = a * b;
//     return result;
//   };
      
//   let product = multiple(2, 5);
//   console.log(product);

//   2. Що таке оператор return в JavaScript? Як його використовувати в функціях?

//   Оператор повернення(return):Використовується для завершення виконання функції та вказує значення,яке повертається викликачеві

//   function add(a , b) {
//     return a + b;
//   }
//   let result = add(5,5);
//   console.log(result)

// 3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?

// Параметри та аргументи функцій є важливими складовими в JavaScript,які дозволяють передачу значень у функції.
// Вони виконують різні ролі та сприяють обміну даними всередині функцій.

// Параметри Функцій: Параметри,Оголошення Функції, Значення-замінники

// Аргументи Функцій:Аргументи,Виклик Функції,Гнучкість Аргументів
// Іноді терміни "аргумент" і "параметр" можуть використовуватися взаємозамінно. Проте, все ж таки вони різні.

// 4. Як передати функцію аргументом в іншу функцію?

// Якщо хочете мати можливість передавати одну функцію в іншу, то для початку ці функції потрібно випередити, а потім уже передати під час виклику, наприклад так:

// function sun() {
//     var x = Math.floor(Math.random() * (4 - 1 + 1)) + 1;
//     console.log(x);
//     return x;
// }

// function mult(func) {
//     var x = 5 + func();
//     console.log(x);
// }
// mult(sun);

// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

// function getFraction(num1, num2) {
//     if (num2 === 0) {
//         return 'Wrong: cannot be divided by zero';
//     }
//     return num1 / num2;
// }

// let fraction = getFraction(21, 8);
// console.log(fraction); 

// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.


function getUserNumber(promptMessage) {
    let user;
    while (true) {
        user = prompt(promptMessage);
        if (user !== null && user !== "" && !isNaN(user)) {
            return parseFloat(user);
        } else if (user === null);
        else {
            alert("Please enter the correct number!");
        }
    }
}

function getOperator() {
    let operator;
    while (true) {
        operator = prompt("Please enter a math operation:");
        if (operator === null);
        else if (['+', '-', '*', '/'].includes(operator)) {
            return operator;
        } else {
            alert("There is no such operation!");
        }
    }
}

function calculate(number1, number2, operator) {
    switch (operator) {
        case '+':
            return number1 + number2;
        case '-':
            return number1 - number2;
        case '*':
            return number1 * number2;
        case '/':
            if (number2 === 0) {
                alert("Division by zero is not possible.");
                return undefined;
            }
            return number1 / number2;
    }
}

function userInteraction() {
    let number1 = getUserNumber("Please enter the first number:");
    let number2 = getUserNumber("Please enter the second number:");
    let operator = getOperator();

    console.log(`The first number: ${number1}`);
    console.log(`The second number: ${number2}`);
    console.log(`Mathematical operation: ${operator}`);

    let result = calculate(number1, number2, operator);

    if (result !== undefined) {
        console.log(`The result of the calculation: ${result}`);
    }
}

userInteraction();
